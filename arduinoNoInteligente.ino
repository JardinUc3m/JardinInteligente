#include <DHTesp.h>
#define DHTPIN 8

//Definimos los pines a usar.

int RelePin = 13;
int SueloPin = A0; 
int sueloRead; 
int ilum;
DHTesp dht;

const int map_low = 700;
const int map_high = 300;

//Inicializamos los pines.

void setup(void) {
Serial.begin(115200);
dht.setup(DHTPIN, DHTesp::DHT11);
pinMode(RelePin, OUTPUT); //modo salida

}

// El programa, lee y si está por debajo del 40% Riega.

void loop(void) {

sueloRead = suelo();
temphum();
Serial.println(" ");
Serial.println("--------------------------");
Serial.println(" ");
if(sueloRead < 40){
  regar();
  Serial.println(" ");
  Serial.println("--------------------------");
  Serial.println(" ");
}
delay(60000);

}

//Funcion que lee Humedad del suelo.

int suelo(){
int analogSuelo = convertToPercent(analogRead(SueloPin));
Serial.print("Suelo = ");
Serial.print(analogSuelo);
Serial.println(" ");
return analogSuelo;
}

//Funcion que lee Humedad y temperatura

void temphum(){
  float h = dht.getHumidity();
  float t = dht.getTemperature();
  Serial.print("Humedad: ");
  Serial.print(h, 2);
  Serial.println("%");
  Serial.print("Temperatura: ");
  Serial.print(t, 2);
  Serial.println("º");
  
}

int convertToPercent(int value){
  int percentValue = 0;
  percentValue = map(value, map_low, map_high, 0, 100);
  return percentValue;
}

//Funcion de regar, Activa el rele hasta que estamos por encima del 90% de humedad en el suelo.

void regar(){
  while(sueloRead < 90){
    digitalWrite(RelePin, HIGH);
    sueloRead = suelo();
    Serial.println(sueloRead);
    delay(2000);
  }
  digitalWrite(RelePin, LOW);
}
